'use strict'
const User = use('App/Models/User')
 
/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.post('/saveTranslitration', async({ request, response }) => {

  let {
    input,
    lang,
    transliteration,
    language
} = request.all();

let translatedata = { input : input, transliteration : transliteration, lang : lang }
translatedata = JSON.stringify(translatedata)

const blog = await User.create({
  translatedata,
  language
});
await blog.save();

return response.send(1);
})

Route.get('/download', async({ request, response }) => {

  let {
    lang
} = request.all();
console.log(lang);
  const ObjectToCsv = function(data){
const csvRows = [];

  //get the headers
const headers = Object.keys(data[0])
csvRows.push(headers.join(','));
  //loop over the rows
for(const row of data){
  const values = headers.map(header => {
    const escaped = (''+row[header]).replace(/"/g, '\\"');
    return `"${escaped}"`;
  });
  csvRows.push(values.join(','));
}
return csvRows.join('\n')

}


let  myCars = await User.query().select('translatedata').where('language', lang).fetch()
myCars = myCars.toJSON()
let json = JSON.parse(myCars[0].translatedata)
const data = myCars.map(row => ({
  lang: json.lang,
  input: json.input,
  transliteration: json.transliteration
}))
const csvData = ObjectToCsv(data);
  return response.send(csvData)
})